/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Client;
import InterfaceApp.InterfaceApp;
import java.rmi.Naming;
import java.util.Scanner;
/**
 *
 * @author Asusazz
 */
public class Client {
      public static void main(String[] args) {
        try {
            //InterfaceApp service1 = (InterfaceApp) Naming.lookup("rmi://127.0.0.1:1250/server");
            //System.out.println(service1.sayHello());
            Scanner myObj = new Scanner(System.in); 
            System.out.println("Enter une chaine de caractère : ");
            String ch = myObj.nextLine();  
            System.out.println("ch : " + ch);  
            InterfaceApp service2 = (InterfaceApp) Naming.lookup("rmi://127.0.0.1:1250/server");
            if(service2.palindromme(ch)==false) System.out.println("la chaine n'est pas palindromme");
            else System.out.println("la chaine est palindromme");

        } catch (Exception e) {
            System.out.println("Error");
            System.out.println(e.toString());
        }
    }
}
