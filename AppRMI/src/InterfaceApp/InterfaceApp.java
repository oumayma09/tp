/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfaceApp;
  
import java.rmi.Remote;
import java.rmi.RemoteException;
/**
 *
 * @author Asus
 */
public interface InterfaceApp extends Remote {
    String sayHello()throws RemoteException;
    boolean palindromme(String ch )throws RemoteException;
}
