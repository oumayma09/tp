/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Serveur;

import InterfaceApp.InterfaceApp;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class Server extends UnicastRemoteObject implements InterfaceApp {

    public Server() throws RemoteException {}

    @Override
    public String sayHello() {
        return "Hello World !!";
    }

    public boolean palindromme(String ch){
      String inv  = "" ;
      
      int length = ch.length();   
      for ( int i = length - 1; i >= 0; i-- )  
         inv = inv + ch.charAt(i);  
      return ch.equals(inv);
   }  
    public static void main(String[] args) throws RemoteException, MalformedURLException {
        Server server = new Server();

        java.rmi.registry.LocateRegistry.createRegistry(1250);

        Naming.rebind("rmi://127.0.0.1:1250/server", server);
    }
}
