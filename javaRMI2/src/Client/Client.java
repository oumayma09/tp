package Client;

import Serveur.MyInterface;
import Serveur.Compte;
import java.rmi.Naming;
import java.util.Scanner;


public class Client {

    public static void main(String[] args) {
        try {
            Scanner myObj = new Scanner(System.in); 
            double solde = myObj.nextFloat();
            Compte compteClt = new Compte(500);
            MyInterface service1 = (MyInterface) Naming.lookup("rmi://127.0.0.1:1250/server");
            System.out.println(service1.lireSolde(compteClt));

        } catch (Exception e) {
            System.out.println("Error");
            System.out.println(e.toString());
        }
    }

}