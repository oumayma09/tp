package Serveur;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.registry.LocateRegistry;

public class Serveur extends UnicastRemoteObject implements MyInterface {

    public Serveur() throws RemoteException{}
    @Override
    public String sayHello() {
        return "Hello World !!";
    }
    @Override
    public void crediter(Compte c,double x){
        c.deposer(x);
    }
    @Override
    public void debiter(Compte c,double x){
        c.retirer(x);
    }
    @Override
    public double lireSolde(Compte c){
        return c.getSolde();
    }
    }

     