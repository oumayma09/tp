package Serveur;

import Serveur.Compte;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface MyInterface extends Remote {

    String sayHello()throws RemoteException;
    void crediter(Compte c,double x) throws RemoteException;
    void debiter(Compte c,double x) throws RemoteException;
    double lireSolde(Compte c) throws RemoteException;
    
}