import java.rmi.Remote;
import java.rmi.RemoteException;

public interface MyInterface extends Remote {

    String sayHello()throws RemoteException;
    String majuscule(String ch )throws RemoteException;
    String date() throws RemoteException;
    boolean estPremier(int nb) throws RemoteException;
    void infPremier(int nombre) throws RemoteException;
}