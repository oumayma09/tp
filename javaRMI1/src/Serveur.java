import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Serveur extends UnicastRemoteObject implements MyInterface {

    public Serveur() throws RemoteException {}

    @Override
    public String sayHello() {
        return "Hello World !!";
    }
    @Override
    public String majuscule(String chaine){
        return chaine.toUpperCase();
    }
    @Override
    public String date(){
        DateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        return format.format(date) ;
    }
    public boolean estPremier(int nb) {
        if(nb>0){
            if (nb == 1) { return false; } 
        for (int i=2; i<= nb/2; i++) { 
            if (nb%i == 0) { 
                return false; 
            } 
        } 
        return true; 
        } 
        return false;
    }
    @Override
    public void infPremier(int nombre){
        for(int i=2;i<=nombre;i++){
            if(estPremier(i)){ System.out.println(i);}
        }
    }
    public static void main(String[] args) throws RemoteException, MalformedURLException {
        Serveur server = new Serveur();

        java.rmi.registry.LocateRegistry.createRegistry(1250);

        Naming.rebind("rmi://127.0.0.1:1250/server", server);
    }
}