import java.rmi.Naming;
import java.util.Scanner;


public class Client {

    public static void main(String[] args) {
        try {
            Scanner myObj = new Scanner(System.in); 
            System.out.println("Enter une chaine de caractère: ");
            String ch = myObj.nextLine();  
            MyInterface service1 = (MyInterface) Naming.lookup("rmi://127.0.0.1:1250/server");
            System.out.println(service1.majuscule(ch));
            int a=5;
            if(service1.estPremier(a)==true) System.out.println("ce nombre n'est pas premier");
            else System.out.println("ce nombre est premier");
            System.out.println(service1.sayHello());

        } catch (Exception e) {
            System.out.println("Error");
            System.out.println(e.toString());
        }
    }

}